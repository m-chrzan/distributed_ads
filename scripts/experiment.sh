prefix=data/$1

export NUMBER_MESSAGES=100

export USE_REDIS=1

python src/enhancer.py &
python src/advertizer.py > $prefix-redis-publish_times &
sleep 1
python src/informer.py > $prefix-redis-inform_times
sleep 1

python scripts/analysis.py $prefix-redis-inform_times $prefix-redis-publish_times > $prefix-redis-analysis

export USE_REDIS=0
python src/enhancer.py &
python src/advertizer.py > $prefix-postgres-publish_times &
sleep 1
python src/informer.py > $prefix-postgres-inform_times
sleep 1

python scripts/analysis.py $prefix-postgres-inform_times $prefix-postgres-publish_times > $prefix-postgres-analysis
