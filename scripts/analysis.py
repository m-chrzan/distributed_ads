import sys

import numpy as np

if len(sys.argv) < 3:
    print('USAGE: analaysis <inform times file> <publish times file>')
    sys.exit(1)

inform_times = {}
publish_times = {}

with open(sys.argv[1]) as f:
    for line in f:
        ident, time = line.split(' ')
        inform_times[ident] = int(time)

with open(sys.argv[2]) as f:
    for line in f:
        ident, time = line.split(' ')
        publish_times[ident] = int(time)

deltas = [(publish_times[ident] - inform_times[ident]) / 1000000 for ident in publish_times]

print('95%-ile:', np.percentile(deltas, 95))
print('average:', np.average(deltas))
print('median:', np.median(deltas))
print('max:', np.max(deltas))
print('min:', np.min(deltas))
print('deltas:')
for delta in deltas:
    print(delta)
