import os

# Default config
use_redis = True
number_messages = 100
should_wait_probability = 0.9
should_emit_probability = 0.6

if os.environ['USE_REDIS']:
    use_redis = os.environ['USE_REDIS'] == '1'

if os.environ['NUMBER_MESSAGES']:
    number_messages = int(os.environ['NUMBER_MESSAGES'])
