class Client:
    def subscribe_opportunities(self):
        """ Subscribes to messages about new advertisment opportunities. """
        pass

    def subscribe_both(self):
        """
        Subscribes to both messages about new advertisment opportunities and
        messages about location information.
        """
        pass

    def get_message(self):
        """
        Returns the next message from one of the subscribed channels, blocking
        until one is available.
        """
        pass

    def get_opportunity(self, ident):
        """ Returns the data for a given opportunity """
        pass

    def publish_opportunity(self, cookie, ip):
        """
        Publishes a new ad emission opportunity. It should cause the underlying
        DB to notify any clients of the opportunity arising.
        """
        pass

    def publish_location(self, ident, location):
        """ Adds location information to the given ad opportunity. """
        pass

    def emit_ad(self, ident, ad):
        """ Specifies the ad to be published for the given ad opportunity. """
        pass
