import config
import utils

c = utils.get_client()

c.subscribe_opportunities()

for i in range(config.number_messages):
    ident = c.get_message()['ident']
    opportunity = c.get_opportunity(ident)
    c.publish_location(ident, utils.generate_location(1, 2))
