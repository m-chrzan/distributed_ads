import random

import redis
import psycopg2

import config
import redis_client
import pg_client

def get_client():
    if config.use_redis:
        r = redis.Redis(
            decode_responses=True,
            unix_socket_path='/var/run/redis/redis-server.sock'
        )
        return redis_client.RedisClient(r)
    else:
        conn = psycopg2.connect("dbname=m user=m")
        return pg_client.PostgresClient(conn)

alphabet = [chr(n) for n in range(ord('a'), ord('z')+1)]

def generate_cookie():
    return ''.join(random.choices(alphabet, k=12))

def generate_ip():
    return '.'.join([str(random.randint(0, 255)) for _ in range(4)])

states = [
    'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID',
    'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS',
    'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK',
    'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV',
    'WI', 'WY'
]

cities = [
    'Johnstown', 'Jamestown', 'Glennville', 'Woodland', 'Collins', 'Hiltop',
    'Enfield', 'Davis', 'Pittsburg', 'San Juan', 'Livermore', 'Tahawus',
    'Mortimer', 'Newville', 'New Hampton'
]

def generate_location(cookie, ip):
    return '{}, {}'.format(random.choice(cities), random.choice(states))

def should_wait(cookie, ip):
    return random.random() < config.should_wait_probability

def should_emit(cookie, ip, location):
    return random.random() < config.should_emit_probability
