import client

class RedisClient(client.Client):
    def __init__(self, r):
        self.r = r
        self.p = self.r.pubsub(ignore_subscribe_messages=True)

    def subscribe_opportunities(self):
        self.p.subscribe('opportunity')

    def subscribe_both(self):
        self.p.subscribe('opportunity', 'location')

    def get_message(self):
        msg = next(self.p.listen())
        return {
            'type': msg['channel'],
            'ident': msg['data']
        }

    def get_opportunity(self, ident):
        data = self.r.hmget(
                'opportunity:{}'.format(ident),
                ['cookie', 'ip', 'location']
        )
        return {
            'cookie': data[0],
            'ip': data[1],
            'location': data[2]
        }

    def publish_opportunity(self, cookie, ip):
        ident = self.r.incr('last-opportunity')
        self.r.hmset(
                'opportunity:{}'.format(ident),
                {
                    'cookie': cookie,
                    'ip': ip
                }
        )
        self.r.publish('opportunity', ident)
        return ident

    def publish_location(self, ident, location):
        self.r.hset(
                'opportunity:{}'.format(ident),
                'location', location
        )
        self.r.publish('location', ident)

    def emit_ad(self, ident, ad):
        self.r.set('ad:{}'.format(ident), ad)
