import time

import config
import utils

c = utils.get_client()
c.subscribe_both()

wait_for_location = {}

times = {}
number_handled = 0

for i in range(2 * config.number_messages):
    msg = c.get_message()
    ident = msg['ident']
    if msg['type'] == 'opportunity':
        opportunity = c.get_opportunity(ident)
        if utils.should_wait(1, 2):
            wait_for_location[ident] = True
        else:
            wait_for_location[ident] = False
            c.emit_ad(ident, 42)
            publish_time = time.time_ns()
            times[ident] = publish_time
    else: # This is a location message
        if wait_for_location[ident]:
            opportunity = c.get_opportunity(ident)
            if utils.should_emit(1, 2, 3):
                c.emit_ad(ident, 42)
                publish_time = time.time_ns()
                times[ident] = publish_time
        del wait_for_location[ident]

for ident in list(times):
    print(ident, times[ident])
times = {}
