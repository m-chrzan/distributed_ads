import select

import client

class PostgresClient(client.Client):
    def __init__(self, conn):
        self.conn = conn
        self.conn.autocommit = True
        self.cur = self.conn.cursor()

    def subscribe_opportunities(self):
        self.cur.execute('LISTEN opportunity;')

    def subscribe_both(self):
        self.cur.execute('LISTEN opportunity;')
        self.cur.execute('LISTEN location;')

    def get_message(self):
        if not self.conn.notifies:
            select.select([self.conn], [], [])
            self.conn.poll()
            self.cur.execute('select 1;')
        notify = self.conn.notifies.pop(0)
        return {
            'type': notify.channel,
            'ident': notify.payload
        }

    def get_opportunity(self, ident):
        self.cur.execute("""
            SELECT cookie, ip, location FROM opportunities
            WHERE id = {}
        """.format(ident))
        row = self.cur.fetchone()
        return {
            'cookie': row[0],
            'ip': row[1],
            'location': row[2]
        }

    def publish_opportunity(self, cookie, ip):
        self.cur.execute("SELECT nextval('last_opportunity');")
        ident = self.cur.fetchone()[0]
        self.cur.execute("""
            INSERT INTO opportunities VALUES ({}, '{}', '{}');
        """.format(ident, cookie, ip)
        )
        self.cur.execute("NOTIFY opportunity, '{}';".format(ident))
        return ident

    def publish_location(self, ident, location):
        self.cur.execute("""
            UPDATE opportunities SET location = '{}' WHERE id = {}
        """.format(location, ident)
        )
        self.cur.execute("NOTIFY location, '{}';".format(ident))

    def emit_ad(self, ident, ad):
        self.cur.execute("""
            INSERT INTO ads VALUES ({}, {})
        """.format(ident, ad)
        )
