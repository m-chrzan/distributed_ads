import time

import config
import utils

c = utils.get_client()

times = {}

for i in range(config.number_messages):
    opportunity_time = time.time_ns()
    ident = c.publish_opportunity(utils.generate_cookie(), utils.generate_ip())
    times[ident] = opportunity_time

for ident in list(times):
    print(ident, times[ident])
