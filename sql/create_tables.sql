CREATE SEQUENCE last_opportunity;
CREATE TABLE opportunities (
    id INT NOT NULL PRIMARY KEY,
    cookie TEXT,
    ip TEXT,
    location TEXT
);
CREATE TABLE ads (
    opportunity_id INT,
    ad_id INT
);
